"""
    Fetches Data either using Data Connector or Request Manager
"""

import os
import shutil
import click
import xpresso.ai.core.data.versioning.controller_factory as version_controller_factory
from xpresso.ai.client.controller_client import ControllerClient
from xpresso.ai.core.data.connections.connector import Connector as cf
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    CLICommandFailedException
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.client.data_client import config

__all__ = "data_cli"
__author__ = "Sanyog Vyawahare"


class DataVersioning:
    """
        To push and pull data from pachyderm cluster
    """
    COMMAND_KEY_WORD = "command"
    REPO_ARGUMENT = "repo"
    COMMIT_ARGUMENT = "commit"
    DESCRIPTION_ARGUMENT = "desc"
    IN_PATH_ARGUMENT = "inpath"
    OUT_PATH_ARGUMENT = "outpath"
    BRANCH_ARGUMENT = "branch"
    DATASET_ARGUMENT = "dataset"
    DATASET_NAME = 'dataset_name'

    TABLE_ARGUMENT = 'table'
    COLUMN_ARGUMENT = 'columns'
    TYPE = "type"
    DELIMITER = "delimiter"

    ENV_ARGUMENT = "environment"

    def __init__(self):
        self.cli_args = {}
        self.arguments = {}
        self.supported_commands = {}
        self.initialize_commands()
        self.logger = XprLogger()
        self.repomanager = None
        self.xpr_con = None
        self.config = config

    def import_data(self):
        """
        Import data from alluxio or presto
        :return:
            Returns a pandas DataFrame and output path
        """

        if not self.cli_args['out_path']:
            raise CLICommandFailedException("Output path mandatory, Please specify using -out or --outpath")

        if self.cli_args['in_path']:
            user_config = {
                "file_name": self.cli_args['in_path']
            }
            self.xpr_con = cf.getconnector(self.cli_args['type'])
            df = self.xpr_con.import_dataframe(user_config=user_config)
        else:
            if not self.cli_args['column']:
                raise CLICommandFailedException("Specify Columns to fetch")
            if not self.cli_args['table']:
                raise CLICommandFailedException("Please provide the table name")
            user_config = {
                "type": self.cli_args['type'],
                "description": self.cli_args['desc'],
                "table": self.cli_args['table'],
                "columns": self.cli_args['column']
            }
            df = self.xpr_con.import_data(user_config)

        return df

    def import_data_as_excel(self):
        """
        Saves df as excel file
        """
        if not self.cli_args['in_path']:
            self.cli_args['in_path'] = '/' + self.cli_args['table'] + '.xlsx'

        df = self.import_data()

        if not os.path.exists(self.cli_args['out_path']):
            os.makedirs(self.cli_args['out_path'])
        df.to_excel(os.path.join(self.cli_args['out_path'], os.path.basename(self.cli_args['in_path'])))

    def import_data_as_csv(self):
        """
        Saves df as csv file
        """
        if not self.cli_args['in_path']:
            self.cli_args['in_path'] = '/' + self.cli_args['table'] + '.csv'

        df = self.import_data()

        if not os.path.exists(self.cli_args['out_path']):
            os.makedirs(self.cli_args['out_path'])
        df.to_csv(os.path.join(self.cli_args['out_path'], os.path.basename(self.cli_args['in_path'])))

    def data_push(self):
        """
        Push the data into the pachyderm repo
        :argument:
            repo_name:
                name of the repo
            branch_name:
                name of the branch
            dataset:
                AbstractDataset object with info on dataset
            dataset_name:
                name of dataset that will be pushed to cluster
            input_path:
                local path of file or list of files i.e a directory
            description:
                A brief description on this push
            type:
                Fetching dataset or files
                Example
                    1. --type "None"  #For pushing dataset
                    2. --type "files"
            env:
                Enviornment for xpresso
        :return:
            new commit id and path of data on cluster
        """
        passw = self.config.PASSWORD
        uid = self.config.UID

        self.version_connect(uid, passw, self.cli_args['env'])
        if self.cli_args['type'].lower() == 'files':

            new_commit_id, path_on_cluster = self.repomanager.push_dataset(
                repo_name=self.cli_args['repo_name'], branch_name=self.cli_args['branch_name'],
                dataset_name=self.cli_args['dataset_name'], path=self.cli_args['in_path'],
                description=self.cli_args['desc'], data_type=self.cli_args['type'])
        else:
            new_commit_id, path_on_cluster = self.repomanager.push_dataset(
                repo_name=self.cli_args['repo_name'], branch_name=self.cli_args['branch_name'],
                dataset=self.cli_args['dataset'], description=self.cli_args['desc'])

        return new_commit_id, path_on_cluster

    def data_pull(self):
        """
        Pulls the data from the pachyderm repo
        :argument:
        Arguments to be passed
            repo_name:
                Name of the repo
            branch_name:
                Branch name
            outpath:
                Output path to store data
            type:
                Fetching dataset or files
                Example
                    1. --type "dataset"
                    2. --type "files"
            inpath:
                Path on the pachyderm cluster(optional)
            commit:
                Commit id of the data push (optional)
            env:
                Enviornment for xpresso
        :return:
         """
        passw = self.config.PASSWORD
        uid = self.config.UID

        self.version_connect(uid, passw, self.cli_args['env'])

        if not self.cli_args['in_path']:
            self.cli_args['in_path'] = '/'

        if not os.path.exists(self.cli_args['out_path']):
            os.makedirs(self.cli_args['out_path'])

        if self.cli_args['type'].lower() == 'dataset':
            dataset = self.repomanager.pull_dataset(
                repo_name=self.cli_args['repo_name'], branch_name=self.cli_args['branch_name'],
                commit_id=self.cli_args['commit_id'], path=self.cli_args['in_path'])
            new_dir_path = dataset.name
            new_dir_path += '.csv'

            dataset.data.to_csv(os.path.join(self.cli_args['out_path'], new_dir_path))

        elif self.cli_args['type'].lower() == 'files':
            print(self.cli_args.items())
            new_dir_path = self.repomanager.pull_dataset(
                repo_name=self.cli_args['repo_name'], branch_name=self.cli_args['branch_name'],
                commit_id=self.cli_args['commit_id'], path=self.cli_args['in_path'], output_type="files")

            print(f"Data Pull from {new_dir_path} to {self.cli_args['out_path']}")
            # os.system(f"ls -laR {new_dir_path}")
            updated_new_dir_path = os.path.join(new_dir_path, self.cli_args['in_path'].lstrip('/'))
            if self.cli_args['out_path']:
                print(f"Copying {updated_new_dir_path} -> {self.cli_args['out_path']}")
                destination_folder = os.path.join(self.cli_args['out_path'], updated_new_dir_path.split("/")[-1])
                print(f"Removing if exists {destination_folder}")
                os.system(f'rm -rf {destination_folder}')
                shutil.move(updated_new_dir_path, self.cli_args['out_path'])

                print(f"Copy {updated_new_dir_path} -> {self.cli_args['out_path']} Done")
        os.system(f'rm -rf {self.cli_args["commit_id"]}')
        return

    def execute(self, **kwargs):
        """
        Validates the command provided and calls the relevant function for
        execution

        :param kwargs:
            It takes kwargs as argument which should contain the
            argument passed in command line
        """
        self.arguments = kwargs
        if self.COMMAND_KEY_WORD not in self.arguments:
            raise CLICommandFailedException("No valid command provided")

        command = self.arguments[self.COMMAND_KEY_WORD]

        if command not in self.supported_commands:
            raise CLICommandFailedException("Invalid command found")
        try:
            self.fetch_arguments()
            result = self.supported_commands[command]()
            return result
        except TypeError as exception:
            raise CLICommandFailedException("Command cannot be executed {}".format(exception))

    def extract_argument(self, argument):
        """
        :param argument:
            Name of argument to extract
        :return:
            argument value or None
        """
        if argument in self.arguments:
            return self.arguments[argument]
        return None

    def fetch_and_store(self):
        """
        Fetches data from nfs and stores it in csv file format
        :argument:
            inpath:
                Path of file on nfs
            outpath:
                Path to store the file in csv format
            type:
                Source from where to fetch
            delimiter:
                Field delimiter for output file
        """
        if not os.path.exists(self.cli_args['out_path']):
            os.makedirs(self.cli_args['out_path'])
        if not self.cli_args['delimiter']:
            raise CLICommandFailedException("Please provide the field delimiter for output file")

        user_config = {
            "type": self.cli_args['type'],
            "file_name": self.cli_args['in_path']
        }

        df = self.xpr_con.import_data(user_config=user_config)
        basename = os.path.basename(self.cli_args['in_path'])
        print(os.path.join(self.cli_args['out_path'], basename))
        df.to_csv(os.path.join(self.cli_args['out_path'], basename), sep=self.cli_args['delimiter'], index=False)

    def initialize_commands(self):
        """
        Initialize the supported command variable

        """
        self.supported_commands = {
            "import_csv": self.import_data_as_csv,
            "import_excel": self.import_data_as_excel,

            "data_push": self.data_push,

            "data_pull": self.data_pull,

            "fetch_and_store": self.fetch_and_store
        }

    def fetch_arguments(self):
        """
        Fetch arguments form CLI
        :return:
            Returns arguments
        """

        self.cli_args['repo_name'] = self.extract_argument(self.REPO_ARGUMENT)
        self.cli_args['commit_id'] = self.extract_argument(self.COMMIT_ARGUMENT)
        self.cli_args['desc'] = self.extract_argument(self.DESCRIPTION_ARGUMENT)
        self.cli_args['in_path'] = self.extract_argument(self.IN_PATH_ARGUMENT)
        self.cli_args['out_path'] = self.extract_argument(self.OUT_PATH_ARGUMENT)
        self.cli_args['branch_name'] = self.extract_argument(self.BRANCH_ARGUMENT)
        self.cli_args['dataset'] = self.extract_argument(self.DATASET_ARGUMENT)
        self.cli_args['dataset_name'] = self.extract_argument(self.DATASET_NAME)

        self.cli_args['table'] = self.extract_argument(self.TABLE_ARGUMENT)
        self.cli_args['column'] = self.extract_argument(self.COLUMN_ARGUMENT)
        self.cli_args['type'] = self.extract_argument(self.TYPE)
        self.cli_args['delimiter'] = self.extract_argument(self.DELIMITER)

        self.cli_args['env'] = self.extract_argument(self.ENV_ARGUMENT)

    def version_connect(self, uid, passw, env):
        try:
            client = ControllerClient()
            client.login(uid, passw)
            controller_factory = version_controller_factory.VersionControllerFactory()
            self.repomanager = controller_factory.get_version_controller()
        except Exception as exp:
            raise exp


@click.command()
@click.argument('command')
@click.option('-r', '--repo', type=str, help='Name of the repo')
@click.option('-b', '--branch', type=str, help='Branch name')
@click.option('-c', '--commit', type=str, help='Commit Id')
@click.option('-d', '--dataset', help='Dataset object')
@click.option('--dname', '--dataset_name', help="Name of the dataset that will be pushed")
@click.option('-out', '--outpath', type=str,
              help='Path where you want to pull')
@click.option('-in', '--inpath', type=str,
              help='Path of the file you want to fetch')
@click.option('--desc', type=str, help='Description regarding the function to perform')
@click.option('-t', '--table', type=str, help='Name of the table in presto data source')
@click.option('-col', '--columns', type=str,
              help='Name of column inside the table stored in presto data source')
@click.option('--type', type=str, help='Source where to pick data from')
@click.option('--delimiter', type=str, help="Field delimiter used for output file")
@click.option('-env', '--environment', type=str, help="Workspace on xpresso")
def cli_options(**kwargs):
    result = DataVersioning()
    try:
        result.execute(**kwargs)

    except Exception as exception:
        click.secho(f"Error:{exception}", err=True, fg="red")


if __name__ == "__main__":
    cli_options()
